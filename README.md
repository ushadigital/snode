![SNode](https://www.freelogoservices.com/api/main/images/1j+ojVVCOMkX9Wyrexe4hGfK8KnU+W0MjkLP2ig3M2RE...QxumCgphfBr47hlc1xFtFwKhRYKdsQ5iSZ8R9FL00QsomDIeoZC)

## About SNode

SNode is a application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. SNode takes the pain out of development by easing common tasks used in many web projects, such as:

- Simple, fast routing engine.
- Multiple back-ends for session and cache storage.
- Expressive, intuitive [database ORM](https://sequelize.org/).
- Database migrations.
- Real-time event broadcasting.

SNode is accessible, powerful, and provides tools required for large, robust applications.

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Structure node application
* Version 0.5
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Requirments ###

* Node.Js 12.x or upper
* Mysql or Postgres
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* git clone https://ujjwalbera@bitbucket.org/ushadigital/snode.git
* npm install
* npm install --save-dev sequelize-cli
* npx sequelize-cli db:migrate
* npx sequelize-cli db:seed:all
* npx nodemon server

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Ujibaba](https://bitbucket.org/ushadigital/)
* pratimmallick, Chandan Mondal